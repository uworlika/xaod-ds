#ifndef AppUtils_TLogFile_H
#define AppUtils_TLogFile_H

//ROOT includes:
#include <RtypesCore.h>

// STL includes:
#include <string>
#include <vector>

namespace AppUtils {

class TLogFile {

public:
    explicit TLogFile(std::string_view fileName, short precision = 3);

    TLogFile(const TLogFile &rhs) = delete;

    TLogFile(TLogFile &&rhs) = delete;

    TLogFile &operator=(const TLogFile &rhs) = delete;

    ~TLogFile() = default;

    void append(std::string &&message);

    void append(double value);

    void appendLine(const std::vector<Double_t> &v);

private:
    std::string m_fileName;
    short m_precision;
};

} // name-space AppUtils
#endif // AppUtils_TLogFile_H
