#ifndef AppUtils_TOptions_H
#define AppUtils_TOptions_H

#include <string>
#include <RStringView.h>

namespace AppUtils {

constexpr auto DEFAULT_TREE_NAME = "CollectionTree";
const auto ROOT_FILE_EXT = ".root";
const auto HIST_FILE_PREFIX = "/hist-";

inline std::string trimParentPath(const std::string &fullPath) {
    return fullPath.substr(fullPath.find_last_of('/') + 1);
}

inline std::string extractParentPath(const std::string &fullPath) {
    return fullPath.substr(0, fullPath.find_last_of('/'));
}

class TOptions {
public:
    explicit TOptions(int argc, const char *const argv[]);

    ~TOptions() = default;

    void interprete();

    bool enableMT() const;

    bool isHelpRequested() const;

    bool isQuietModeEnabled() const;

    bool isLogEnabled() const;

    unsigned int NThreads() const;

    unsigned long long maxEvents() const;

    std::string_view fileNamePattern() const;

    std::string_view outDirectory() const;

    std::string_view logfileName() const;

private:
    const int m_argc;
    const char *const *m_argv;

    bool m_enableMT;
    bool m_isHelpRequested;
    bool m_isQuietModeEnabled;
    unsigned int m_NThreads;
    unsigned long long m_maxEvents;
    std::string m_fileNamePattern;
    std::string m_outDirectory;
    std::string m_logfileName;
};

} // name-space AppUtils
#endif // AppUtils_TOptions_H
