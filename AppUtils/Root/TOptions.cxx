#include "AppUtils/TOptions.h"
#include <boost/program_options.hpp>
#include <TError.h>
#include <iostream>

namespace AppUtils {
namespace options = boost::program_options;
using Descriptions = boost::program_options::options_description;

// Let the caller own argv.
TOptions::TOptions(int argc, const char *const argv[]) :
    m_argc{argc}, m_argv{argv},
    m_enableMT{true}, m_isHelpRequested{false}, m_isQuietModeEnabled{false},
    m_NThreads{0}, m_maxEvents{ULONG_LONG_MAX},
    m_fileNamePattern{}, m_outDirectory{}, m_logfileName{} {
}

void TOptions::interprete() {
    const auto APP_NAME = m_argv[0];
    std::string defaultLogFileName{APP_NAME};
    defaultLogFileName += ".InitTime";

    // Declare default command line arguments.
    Descriptions descriptions("Command line options");
    descriptions.add_options()("help,h", "Provide usage help")
        ("input-files,i", options::value<std::string>(&m_fileNamePattern)->default_value("*.root*"),
         "Input filename pattern")
        ("out-dir,o", options::value<std::string>(&m_outDirectory)->default_value("outDir"), "Output directory")
        ("enable-MT,mt", options::value<bool>(&m_enableMT)->default_value(true),
         "[true/false, yes/no] Allows to disable ImpliciteMT feature\nDefault: true\n"
         "When disabled, --threads option is ignored")
        ("quiet,q", options::value<bool>(&m_isQuietModeEnabled)->default_value(false)->implicit_value(true),
         "[true/false, yes/no] Disables printing results to stdout\nDefault: false\n"
         "Reduces verbosity in batch mode/performance tests")
        ("threads,t", options::value<unsigned int>(&m_NThreads)->default_value(m_NThreads),
         "Limit number of threads used for ImpliciteMT feature\nDefault: Use all available threads")
        ("max-events,e", options::value<unsigned long long>(&m_maxEvents)->default_value(m_maxEvents),
         "Limit the number of events to process\nDefault: Process all events")
        ("logfile,l", options::value<std::string>(&m_logfileName), "Logfile name: Currently used for instrumentation"
                                                                   " of performance time steps.");

    // Interpret command line arguments.
    options::variables_map parameters;
    try {
        options::store(options::parse_command_line(m_argc, m_argv, descriptions), parameters);
        options::notify(parameters);
    } catch (const std::exception &ex) {
        Error(APP_NAME, "Failed to parse command line arguments\n: %s", ex.what());
    }

    if (parameters.count("help")) {
        m_isHelpRequested = true;
        std::cout << "Usage: " << APP_NAME << " [--option1 value1, ..., --optionN valueN]\n";
        std::cout << descriptions << std::endl;
    }
}

bool TOptions::enableMT() const { return m_enableMT; }

unsigned int TOptions::NThreads() const { return m_NThreads; }

unsigned long long TOptions::maxEvents() const { return m_maxEvents; }

std::string_view TOptions::fileNamePattern() const { return m_fileNamePattern; }

std::string_view TOptions::outDirectory() const { return m_outDirectory; }

bool TOptions::isHelpRequested() const { return m_isHelpRequested; }

bool TOptions::isQuietModeEnabled() const { return m_isQuietModeEnabled; }

bool TOptions::isLogEnabled() const { return !(m_logfileName.empty()); }

std::string_view TOptions::logfileName() const { return m_logfileName; }

} //name-space AppUtils