#include "AppUtils/TOptions.h"
#include "AppUtils/TLogFile.h"

// STL includes:
#include <mutex>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <iostream>
#include <iomanip>

namespace AppUtils {

std::mutex s_logMutex;

TLogFile::TLogFile(std::string_view fileName, short precision) :
    m_fileName{fileName.data()}, m_precision{precision} {
}

void TLogFile::append(std::string &&message) {
    if (message.empty()) { return; }

    std::fstream logFile;
    std::lock_guard<std::mutex> lock(s_logMutex);
    logFile.open(m_fileName, std::fstream::app);
    logFile << std::forward<std::string>(message);
    logFile.close();
}

void TLogFile::append(double value) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(m_precision) << value << "\t";
    append(ss.str());
}

// A convenience method that accepts a vector and appends its contents as tab-space delimited values.
void TLogFile::appendLine(const std::vector<Double_t> &values) {
    if (values.empty()) { return; }

    std::stringstream ss;
    for (const auto &v: values) {
        ss << std::fixed << std::setprecision(m_precision) << v << "\t";
    }

    ss.str().pop_back();
    ss << std::endl;
    append(ss.str());
}

} //name-space AppUtils