#ifndef xAODdataSource_LinkDef_H
#define xAODdataSource_LinkDef_H

// Some common definitions:
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

// Declare the class(es) to generate dictionaries for:
#pragma link C++ class TxAODdataSource+;
#pragma link C++ class TEventCursor+;
#pragma link C++ class IHandle+;
#pragma link C++ class THandle+;
#pragma link C++ class TLapTimer+;

#endif // xAODdataSource_LinkDef_H
