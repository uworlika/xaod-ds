#include "xAODdataSource/TxAODdataSource.h"

//ROOT includes:
#include <ROOT/TSeq.hxx>
#include <TChainElement.h>

//STL includes:
#include <fstream>
#include <utility>

namespace xAOD {
namespace DataSource {

/// The parameter lapTimer shared pointer is passed by value to keep it as an optional feature.
/// This allows data source to keep this member uninitialized by default. The initializer list moves it to prevent
/// parameter copy, thus eliminates a redundant increment/decrement of it's shared reference count.
TxAODdataSource::TxAODdataSource(std::string_view fileNamePattern,
                                 std::string_view eventTreeName,
                                 std::shared_ptr<TLapTimer> lapTimer) :
    m_fileNamePattern{fileNamePattern.data()},
    m_eventTreeName{eventTreeName.data()},
    m_lapTimer{std::move(lapTimer)} {
    InitMetaData();
}

TxAODdataSource::~TxAODdataSource() {
    m_entryRanges.clear();
    m_columnClassMap.clear();
    m_columnNames.clear();
    m_eventCursors.clear();
}

void TxAODdataSource::SetNSlots(unsigned int nSlots) {
    if (nSlots == 0) { throw std::invalid_argument("Requested number of slots is zero."); }

    m_NSlots = nSlots;
    m_eventCursors.resize(m_NSlots);
    for (auto const slot : ROOT::TSeqU(m_NSlots)) {
        auto eventCursor = std::make_unique<TEventCursor>();
        m_eventCursors[slot] = std::move(eventCursor);
    }
}

void TxAODdataSource::Initialise() {
    const auto &chain = makeDataChain(m_fileNamePattern, m_eventTreeName);
    const auto &chainElements = chain->GetListOfFiles();
    TIter next(chainElements);
    TChainElement *chainElement = nullptr;
    Long64_t fileOffset = 0;    //Number of events from beginning of event data.

    //Collect all clusters from each tree. Each chain element maps one tree with its owner file.
    while ((chainElement = (TChainElement *) next()) != nullptr) {
        std::string_view fileName(chainElement->GetTitle());
        const auto &file = std::unique_ptr<TFile>(TFile::Open(fileName.data()));
        if (file->IsZombie()) {
            std::ostringstream message{"Failed to open file: "};
            message << fileName;
            throw std::runtime_error(message.str());
        }

        TTree *tree = nullptr; //file owns this tree.
        file->GetObject(m_eventTreeName.data(), tree);

        Long64_t clusterStart = 0, clusterEnd = 0;
        const auto entries = tree->GetEntries();
        auto clusterIter = tree->GetClusterIterator(0);
        while ((clusterStart = clusterIter()) < entries) {
            clusterEnd = clusterIter.GetNextEntry();
            m_entryRanges.emplace_back(fileOffset + clusterStart, fileOffset + clusterEnd);
        }

        fileOffset += entries;
    }
    m_NPendingTasks = m_entryRanges.size();
    if (m_lapTimer) { m_lapTimer->split("EventLoop"); }
}

void TxAODdataSource::Finalise() {
    m_entryRanges.clear();
    //Note: EventCursors need not be finalised here. They are finalised for their corresponding slots.
    // i.e. After the last task on given slot is done.
}

bool TxAODdataSource::HasColumn(std::string_view columnName) const {
    return (m_columnClassMap.find(columnName.data()) != m_columnClassMap.end());
}

const std::vector<std::string> &TxAODdataSource::GetColumnNames() const {
    return m_columnNames;
}


std::string TxAODdataSource::GetTypeName(std::string_view columnName) const {
    if (!HasColumn(columnName)) {
        throw std::runtime_error("Requested column does not exist in DataSource.");
    }

    const auto columnClassPair = *(m_columnClassMap.find(columnName.data()));
    return columnClassPair.second;
}

std::vector<std::pair<ULong64_t, ULong64_t>> TxAODdataSource::GetEntryRanges() {
    //RDataFrame uses task stealing for load balancing.
    //The entry ranges therefore are owned/dynamically updated by RDataFrame after this call.
    //DataSource should not assume/store these values during it's lifetime.
    auto ranges(std::move(m_entryRanges));
    return ranges;
}

std::vector<void *>
TxAODdataSource::GetColumnReadersImpl(std::string_view columnName, const std::type_info &typeInfo) {
    if (!HasColumn(columnName)) {
        throw std::runtime_error("Requested column does not exist in DataSource.");
    }

    std::vector<void *> columnReaders(m_NSlots);
    for (auto const slot : ROOT::TSeqU(m_NSlots)) {
        columnReaders[slot] = m_eventCursors[slot]->columnReader(columnName.data(), typeInfo);
    }
    return columnReaders;
}

///\brief Positions the event cursor of specified slot, to the event specified by entry.
///\returns \true if this event must be processed.
bool TxAODdataSource::SetEntry(unsigned int slot, ULong64_t entry) {
    m_eventCursors[slot]->setEntry(entry);
    return true; // Process all requested events (for now). This could be useful for features like lightweight
    // pre-filters. Skip events based on some meta-data information before reading event record from event store.
}

void TxAODdataSource::InitSlot(unsigned int slot, ULong64_t firstEntry) {
    --m_NPendingTasks;
    if (m_eventCursors[slot]->isInitialized()) { return; }

    //TEvent expects raw pointer to TChain and it owns the chain.
    std::unique_ptr<TChain> chain = makeDataChain(m_fileNamePattern, m_eventTreeName);
    //Todo: Access mode for TEvent initialization needs to be configurable.
    auto event = std::make_unique<TEvent>(chain.release(), TEvent::kClassAccess);
    m_eventCursors[slot]->initialise(std::move(event));

    (void) firstEntry;   // DataSource currently does not need to use firstEntry here.
}

void TxAODdataSource::FinaliseSlot(unsigned int slot) {

    //if (!(m_NPendingTasks % 100)) { std::cout << "Pending Clusters: " << m_NPendingTasks << std::endl; }
    if (m_NPendingTasks == 0) { m_eventCursors[slot]->finalise(); }     // Finalise if no more task to process
}

void TxAODdataSource::InitMetaData() {
    std::unique_ptr<TChain> metaChain = makeDataChain(m_fileNamePattern, m_eventTreeName);
    //Use branch access for TEvent, since only EventFormat information is required for initialisation.
    auto metaEvent = std::make_unique<TEvent>(metaChain.release(), TEvent::kBranchAccess);
    m_size = static_cast<ULong64_t>(metaEvent->getEntries()); //TEvent internally does a range check.
    if (m_size == 0) { throw std::runtime_error("Unable to retrieve event records from event store."); }

    metaEvent->getEntry(0LL);   //Review: Lookup cleaner alternative to connect the TEvent instance with files.
    auto eventFormat = metaEvent->inputEventFormat();

    EventFormat_v1::const_iterator formatIterator;
    for (formatIterator = eventFormat->begin(); formatIterator != eventFormat->end(); ++formatIterator) {
        const auto &formatInfo = (*formatIterator).second;
        const std::string className{formatInfo.className()};
        const std::string columnName{formatInfo.branchName()};

        m_columnClassMap.emplace(columnName, className);
        m_columnNames.emplace_back(columnName);
    }
}

std::unique_ptr<TChain>
makeDataChain(std::string_view fileNamePattern, std::string_view treeName) {
    std::unique_ptr<TChain> chain;
    {
        R__LOCKGUARD(gROOTMutex);
        chain = std::make_unique<TChain>(treeName.data());
    }
    chain->ResetBit(kMustCleanup);      //Informs ROOT that client will trigger release of chains resources.
    chain->Add(fileNamePattern.data());
    return chain;
}

RDataFrame
make_xAODdataFrame(std::string_view fileNamePattern, std::string_view treeName, std::shared_ptr<TLapTimer> lapTimer) {
    //This function will be called by Analysis user, hence the name follows
    // camel-case format, unlike other methods inherited from ROOT's interface.

    if (fileNamePattern.empty()) { throw std::invalid_argument("Filename pattern is empty."); }
    if (treeName.empty()) { throw std::invalid_argument("Tree name is empty."); }

    RDataFrame rdf(std::make_unique<TxAODdataSource>(fileNamePattern, treeName, std::move(lapTimer)));
    return rdf;
}

} // end namespace DataSource
} // end namespace xAOD