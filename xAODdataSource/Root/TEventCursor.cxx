#include "xAODdataSource/TEventCursor.h"

// AnalysisBase includes:
#include "AsgTools/MessageCheck.h"

// STL includes:
#include <iostream>
#include <algorithm>


namespace xAOD {
namespace DataSource {
using namespace asg::msgUserCode;

TEventCursor::~TEventCursor() = default;

void TEventCursor::initialise(std::unique_ptr<TEvent> event) {
    m_event = std::move(event);
}

void TEventCursor::finalise() {
    m_event.reset(nullptr);
    m_columnHandles.clear();
}

void *TEventCursor::columnReader(std::string_view columnName, const std::type_info &typeInfo) {
    //Use existing handle if column is already available, add new otherwise.
    auto columnHandle = std::find_if(m_columnHandles.cbegin(), m_columnHandles.cend(),
                                     [columnName](const std::unique_ptr<IHandle> &h) {
                                         return h->columnName() == columnName;
                                     });

    if (columnHandle == m_columnHandles.cend()) {
        auto newItemStatusPair = m_columnHandles.emplace(makeHandle(columnName, typeInfo, m_event));
        columnHandle = newItemStatusPair.first;

        //Requested handle was not available, and new handle creation failed. This is exceptional case.
        //Let callers know that something seriously went wrong.
        if (!newItemStatusPair.second) {
            std::ostringstream message{"Failed to create a new handle for column: "};
            message << columnName;
            throw std::runtime_error(message.str());
        }
    }

    return (*columnHandle)->reader();
}

int TEventCursor::setEntry(const ULong64_t entry) {
    ANA_CHECK_SET_TYPE(int);
    m_event->getEntry(entry);

    for (auto &columnHandle: m_columnHandles) {
        ANA_CHECK(columnHandle->update());
    }

    return 0;
}

} // end namespace DataSource
} // end namespace xAOD