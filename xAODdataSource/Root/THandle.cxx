#include "xAODdataSource/THandle.h"

// AnalysisBase includes:
#include "xAODCore/AuxContainerBase.h"
#include "AthContainersInterfaces/IAuxElement.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODRootAccess/tools/Utils.h"
#include "AsgTools/MessageCheck.h"

// ROOT includes:
#include <TClass.h>

namespace xAOD {
namespace DataSource {

using namespace asg::msgUserCode;
constexpr auto AUX_ELEMENT = "SG::IAuxElement";
constexpr auto AUX_VECTOR = "SG::AuxVectorBase";

std::unique_ptr<IHandle> makeHandle(std::string_view columnName,
                                    const std::type_info &typeInfo,
                                    const std::unique_ptr<TEvent> &event) {

    std::string normalisedTypeName = extractNormalisedTypeName(typeInfo);
    const auto &type = TClass::GetClass(normalisedTypeName.data());

    if (type->InheritsFrom(AUX_ELEMENT)) {
        return std::make_unique<THandle<SG::IAuxElement>>(columnName, event);
    }

    if (type->InheritsFrom(AUX_VECTOR)) {
        return std::make_unique<THandle<SG::AuxVectorBase>>(columnName, event);
    }

    // Todo: Exotic case, This is more of a requirements question.
    // Understand which other possibilities data source must support.
    std::string errorMessage{"Unable to create handle for column: "};
    throw std::runtime_error(errorMessage += columnName);
}

//Returns the normalised type name, as stored in ROOT's type dictionary.
std::string extractNormalisedTypeName(const std::type_info &typeInfo) {
    //Get the type name in normalised form, exactly as stored in ROOT's class dictionary.
    auto normalisedTypeName = Utils::getTypeName(typeInfo);

    //Attempt to remove all type adorners [e.g. const*, const&] after first whitespace.
    auto fromPosition = normalisedTypeName.find(' ');
    if (fromPosition == std::string::npos)              //Exit early, if the type name contains single literal word.
        return normalisedTypeName;

    normalisedTypeName.erase(fromPosition);
    return normalisedTypeName;
}

// TEventCursor uses this as a link between data access pointer and memory location.
template<typename T>
THandle<T>::THandle(std::string_view columnName, const std::unique_ptr<TEvent> &event) :
    m_columnName{columnName.data()}, m_event{event}, m_address{nullptr} {
    m_link = &m_address;
    m_reader = &m_link;
}

template<typename T>
THandle<T>::THandle(THandle &&rhs) noexcept :
    m_columnName{rhs.m_columnName}, m_event{rhs.m_event}, m_address{std::move(rhs.m_address)} {
    m_link = &m_address;
    m_reader = &m_link;
}

template<typename T>
THandle<T> &THandle<T>::operator=(THandle &&rhs) noexcept {
    m_columnName = std::move(rhs.m_columnName);
    m_address = std::move(rhs.m_address);
    m_link = &m_address;
    m_reader = &m_link;
    return *this;
}

template<typename T>
THandle<T>::~THandle<T>() {
    m_address = nullptr;
    m_link = nullptr;
    m_reader = nullptr;
}

template<typename T>
std::string_view THandle<T>::columnName() const {
    return m_columnName;
}

template<typename T>
void *THandle<T>::reader() const {
    return m_reader;
}

template<typename T>
StatusCode THandle<T>::update() {
    ANA_CHECK(m_event->retrieve(m_address, m_columnName));
    return StatusCode::SUCCESS;
}

template<typename T>
bool THandle<T>::operator==(const IHandle &rhs) const {
    return m_columnName == rhs.columnName();
}

template<typename T>
bool THandle<T>::operator!=(const IHandle &rhs) const {
    return m_columnName != rhs.columnName();
}

template<typename T>
bool THandle<T>::operator<(const IHandle &rhs) const {
    return m_columnName < rhs.columnName();
}

template<typename T>
bool THandle<T>::operator>(const IHandle &rhs) const {
    return m_columnName > rhs.columnName();
}

template<typename T>
bool THandle<T>::operator<=(const IHandle &rhs) const {
    return m_columnName <= rhs.columnName();
}

template<typename T>
bool THandle<T>::operator>=(const IHandle &rhs) const {
    return m_columnName >= rhs.columnName();
}

}// end namespace DataSource
} // end namespace xAOD