#include "xAODdataSource/TLapTimer.h"

// STL includes:
#include <algorithm>
#include <stdexcept>
#include <iostream>

namespace xAOD {
namespace DataSource {


void TLapTimer::start(std::string &&timeLapName) {
    m_wallClock.Start();
    m_stopWatch.Start();
    m_timeLaps.emplace_back(std::forward<std::string>(timeLapName), 0.0);
}

void TLapTimer::split(std::string &&timeLapName) {
    if (m_timeLaps.empty()) { throw std::runtime_error("Unable to split time lap. No active time-lap to split."); }

    m_timeLaps.back().second = m_stopWatch.RealTime();  // Update duration of last time lap, this stops the watch.
    m_stopWatch.Start();
    m_timeLaps.emplace_back(std::forward<std::string>(timeLapName), 0.0);          // Begin a new time lap.
}

void TLapTimer::stop() {
    const auto duration = m_stopWatch.RealTime();                       // Get duration, this stops the watch.
    if (!m_timeLaps.empty()) { m_timeLaps.back().second = duration; }   // Update last time lap.
}

void TLapTimer::reset() {
    m_wallClock.Reset();
    m_stopWatch.Reset();
    std::deque<TimeLap_t>().swap(m_timeLaps);
}

// Returns elapsed time from last start() call of this LapTimer, the wall clock keeps running.
seconds TLapTimer::wallTime() {
    const auto elapsedTime = m_wallClock.RealTime();
    m_wallClock.Continue();
    return elapsedTime;
}

std::vector<std::string> TLapTimer::lapNames() const {
    std::vector<std::string> names{};
    std::transform(m_timeLaps.begin(), m_timeLaps.end(),
                   std::back_inserter(names),
                   [](const TimeLap_t timeLap) { return timeLap.first; });

    return names;
}

std::vector<seconds> TLapTimer::lapDurations() const {
    std::vector<seconds> durations{};
    std::transform(m_timeLaps.begin(), m_timeLaps.end(),
                   std::back_inserter(durations),
                   [](const TimeLap_t timeLap) { return timeLap.second; });

    return durations;
}

} // end namespace DataSource
} // end namespace xAOD