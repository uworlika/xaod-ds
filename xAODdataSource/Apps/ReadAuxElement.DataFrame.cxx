#include "xAODdataSource/TxAODdataSource.h"
#include "AppUtils/TOptions.h"
#include "AppUtils/TLogFile.h"

//AnalysisBase includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/Init.h"
#include "AsgTools/MessageCheck.h"


int main(int argc, char *argv[]) {
    using namespace asg::msgUserCode;
    using EventInfo = xAOD::EventInfo;
    using TH1DModel = ROOT::RDF::TH1DModel;
    using TLapTimer = xAOD::DataSource::TLapTimer;

    ANA_CHECK_SET_TYPE(int);   // Required since the return type is different than standard ATLAS StatusCode.
    std::shared_ptr<TLapTimer> lapTimer;
    AppUtils::TOptions options(argc, argv);
    options.interprete();
    if (options.isHelpRequested()) { return 0; }
    if (options.isLogEnabled()) {
        lapTimer = std::make_shared<TLapTimer>();
        lapTimer->start("Initialisation");
    }

    ANA_CHECK(xAOD::Init(argv[0])); // Set up the job for xAOD access.
    if (options.enableMT()) { ROOT::EnableImplicitMT(options.NThreads()); }
    auto xdf = xAOD::DataSource::make_xAODdataFrame(options.fileNamePattern(),
                                                    xAOD::DataSource::EVENT_TREE_NAME,
                                                    lapTimer);

    //Define transient columns mapping to xAOD functions EventInfo::runNumber, EventInfo::eventNumber
    constexpr auto branchName = "EventInfo";
    constexpr auto runNumber = "RunNumber";
    constexpr auto eventNumber = "EventNumber";
    auto runNumberData = xdf.Define(runNumber, [](const EventInfo *ei) { return ei->runNumber(); }, {branchName});
    auto eventNumberData = xdf.Define(eventNumber, [](const EventInfo *ei) { return ei->eventNumber(); }, {branchName});

    //Apply lazy actions
    auto eventNumberMin = eventNumberData.Min({eventNumber});
    auto eventNumberMax = eventNumberData.Max({eventNumber});
    auto runNumberHist = runNumberData.Histo1D(runNumber);

    //Access data
    const auto eventNumMin = eventNumberMin.GetValue();
    const auto eventNumMax = eventNumberMax.GetValue();

    const auto runNumEntries = runNumberHist->GetEntries();
    const auto runNumMean = runNumberHist->GetMean();
    const auto runNumSD = runNumberHist->GetStdDev();

    if (!options.isQuietModeEnabled()) {
        ANA_MSG_INFO("EventNumber Range: [" << std::setprecision(16) << eventNumMin << ", " << eventNumMax << "]");
        ANA_MSG_INFO("RunNumber entries: " << runNumEntries << ", Mean: " << runNumMean << " +/- " << runNumSD);
    }

    if (options.isLogEnabled()) {
        lapTimer->stop();
        auto lapDurations = lapTimer->lapDurations();

        AppUtils::TLogFile logFile(options.logfileName());
        logFile.append(lapTimer->wallTime());
        logFile.appendLine(lapDurations);
    }

    return 0;
}