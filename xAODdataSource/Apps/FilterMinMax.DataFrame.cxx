#include "xAODdataSource/TxAODdataSource.h"
#include "AppUtils/TOptions.h"
#include "AppUtils/TLogFile.h"

//AnalysisBase includes:
#include "xAODRootAccess/Init.h"
#include "xAODBase/IParticleContainer.h"
#include "AsgTools/MessageCheck.h"

int main(int argc, char *argv[]) {
    using namespace asg::msgUserCode;
    using TLapTimer = xAOD::DataSource::TLapTimer;

    ANA_CHECK_SET_TYPE(int);   // Required since the return type is different than standard ATLAS StatusCode.
    AppUtils::TOptions options(argc, argv);
    std::shared_ptr<TLapTimer> lapTimer;
    options.interprete();
    if (options.isHelpRequested()) return 0;
    if (options.isLogEnabled()) {
        lapTimer = std::make_shared<TLapTimer>();
        lapTimer->start("Initialisation");
    }

    ANA_CHECK(xAOD::Init(argv[0])); // Set up the job for xAOD access.
    if (options.enableMT()) { ROOT::EnableImplicitMT(options.NThreads()); }
    auto xdf = xAOD::DataSource::make_xAODdataFrame(options.fileNamePattern(),
                                                    xAOD::DataSource::EVENT_TREE_NAME,
                                                    lapTimer);

    auto totalEvents = xdf.Count();

    //Define a limiting function for pt variable.
    constexpr auto ptThreshold = 10000.0;
    const auto ptCut = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> ptVector{};
        for (const auto &p: *particles) {
            if (p->pt() >= ptThreshold) { ptVector.push_back(p->pt()); };
        }
        return ptVector;
    };

    //Define a transient column to hold filtered data.
    constexpr auto ptCutColumn = "ptCutVector";
    auto ptCutData = xdf.Define(ptCutColumn, ptCut, {"Electrons"});

    //Define a lazy event filter and use it to derive more analysis variables.
    auto filteredEvents = ptCutData.Filter("ptCutVector.size() > 2");   // String used to test jitting.
    auto filteredEventCount = filteredEvents.Count();
    auto ptCutMin = filteredEvents.Min(ptCutColumn);
    auto ptCutMax = filteredEvents.Max(ptCutColumn);

    //Access data, trigger event loop.
    const auto totalCount = totalEvents.GetValue();
    const auto filteredCount = filteredEventCount.GetValue();
    const auto ptCutMinimum = ptCutMin.GetValue();
    const auto ptCutMaximum = ptCutMax.GetValue();

    if (!options.isQuietModeEnabled()) {
        ANA_MSG_INFO("Total events: " << std::setprecision(16) << totalCount << " Filtered events: " << filteredCount);
        ANA_MSG_INFO("Range|{Electron_pt >= minPt}: " << "[" << ptCutMinimum << ", " << ptCutMaximum << "]");
    }

    if (options.isLogEnabled()) {
        lapTimer->stop();
        auto lapDurations = lapTimer->lapDurations();

        AppUtils::TLogFile logFile(options.logfileName());
        logFile.append(lapTimer->wallTime());
        logFile.appendLine(lapDurations);
    }

    return 0;
}
