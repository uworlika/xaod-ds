#include "xAODdataSource/TxAODdataSource.h"
#include "AppUtils/TOptions.h"

//AnalysisBase includes:
#include "xAODRootAccess/Init.h"
#include "xAODBase/IParticleContainer.h"
#include "AsgTools/MessageCheck.h"
#include "AppUtils/TLogFile.h"

int main(int argc, char *argv[]) {
    using namespace asg::msgUserCode;
    using TH1DModel = ROOT::RDF::TH1DModel;
    using TLapTimer = xAOD::DataSource::TLapTimer;

    ANA_CHECK_SET_TYPE(int);   // Required since the return type is different than standard ATLAS StatusCode.
    AppUtils::TOptions options(argc, argv);
    std::shared_ptr<TLapTimer> lapTimer;
    options.interprete();
    if (options.isHelpRequested()) return 0;
    if (options.isLogEnabled()) {
        lapTimer = std::make_shared<TLapTimer>();
        lapTimer->start("Initialisation");
    }

    ANA_CHECK(xAOD::Init(argv[0])); // Set up the job for xAOD access.
    if (options.enableMT()) { ROOT::EnableImplicitMT(options.NThreads()); }
    auto xdf = xAOD::DataSource::make_xAODdataFrame(options.fileNamePattern(),
                                                    xAOD::DataSource::EVENT_TREE_NAME,
                                                    lapTimer);

    constexpr auto nTransformIterations = 100;
    const auto sequence = ROOT::TSeq<int>(nTransformIterations);
    const auto sin_sqrd_plus_cos_sqrd = [](double x) { return std::pow(std::sin(x), 2) + std::pow(std::cos(x), 2); };

    // Extract vector variables from AuxContainer
    const auto pt = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->pt()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto eta = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->eta()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto phi = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->phi()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto m = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->m()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto e = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->e()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto rapidity = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->rapidity()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto beta = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Beta()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto gamma = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Gamma()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto mag = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Mag()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto px = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Px()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto py = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Py()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    const auto pz = [sequence, sin_sqrd_plus_cos_sqrd](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Pz()); }
        for (const auto n: sequence) { std::transform(v.begin(), v.end(), v.begin(), sin_sqrd_plus_cos_sqrd); }
        return v;
    };

    //Define columns and histograms
    //Todo: Investigate suitable pre-processing to eliminate hardcoded container list.
    std::vector<std::string> containerNames{"Muons", "Photons", "Electrons"};

    std::string columnName;
    TH1DModel histModel;
    constexpr unsigned int nBins = 128;
    constexpr double xLow = 0.0, xUp = 0.0;

    std::vector<ROOT::RDF::RResultPtr<::TH1D>> histograms{};
    for (auto &containerName : containerNames) {
        columnName = containerName + "_ptX";
        auto ptData = xdf.Define(columnName, pt, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(ptData.Histo1D(histModel, columnName));

        columnName = containerName + "_etaX";
        auto etaData = xdf.Define(columnName, eta, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(etaData.Histo1D(histModel, columnName));

        columnName = containerName + "_phiX";
        auto phiData = xdf.Define(columnName, phi, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(phiData.Histo1D(histModel, columnName));

        columnName = containerName + "_mX";
        auto mData = xdf.Define(columnName, m, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(mData.Histo1D(histModel, columnName));

        columnName = containerName + "_eX";
        auto eData = xdf.Define(columnName, e, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(eData.Histo1D(histModel, columnName));

        columnName = containerName + "_rapidityX";
        auto rapidityData = xdf.Define(columnName, rapidity, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(rapidityData.Histo1D(histModel, columnName));

        columnName = containerName + "_betaX";
        auto betaData = xdf.Define(columnName, beta, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(betaData.Histo1D(histModel, columnName));

        columnName = containerName + "_gammaX";
        auto gammaData = xdf.Define(columnName, gamma, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(gammaData.Histo1D(histModel, columnName));

        columnName = containerName + "_magX";
        auto magData = xdf.Define(columnName, mag, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(magData.Histo1D(histModel, columnName));

        columnName = containerName + "_pxX";
        auto pxData = xdf.Define(columnName, px, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(pxData.Histo1D(histModel, columnName));

        columnName = containerName + "_pyX";
        auto pyData = xdf.Define(columnName, py, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(pyData.Histo1D(histModel, columnName));

        columnName = containerName + "_pzX";
        auto pzData = xdf.Define(columnName, pz, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(pzData.Histo1D(histModel, columnName));
    }

    if (!options.isQuietModeEnabled()) {
        std::cout << std::setw(20) << std::left << "Histogram" << std::setw(16) << "Entries" << "Mean\n";
    }

    for (auto &h: histograms) {     //Access data, Trigger event loop
        const auto histName = h->GetName();
        const auto entries = h->GetEntries();
        const auto mean = h->GetMean();

        if (options.isQuietModeEnabled()) continue;
        std::cout << std::setw(20) << std::left << histName
                  << std::setw(16) << entries << mean << "\n";
    }

    if (options.isLogEnabled()) {
        lapTimer->stop();
        auto lapDurations = lapTimer->lapDurations();

        AppUtils::TLogFile logFile(options.logfileName());
        logFile.append(lapTimer->wallTime());
        logFile.appendLine(lapDurations);
    }

    return 0;
}