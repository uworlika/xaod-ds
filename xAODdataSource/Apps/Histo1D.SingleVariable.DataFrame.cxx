#include "xAODdataSource/TxAODdataSource.h"
#include "AppUtils/TOptions.h"
#include "AppUtils/TLogFile.h"

//AnalysisBase includes:
#include "xAODRootAccess/Init.h"
#include "xAODBase/IParticleContainer.h"
#include "AsgTools/MessageCheck.h"

//ROOT includes:
#include <ROOT/RDFInterface.hxx>

int main(int argc, char *argv[]) {
    using namespace asg::msgUserCode;
    using TH1DModel = ROOT::RDF::TH1DModel;
    using TLapTimer = xAOD::DataSource::TLapTimer;

    ANA_CHECK_SET_TYPE(int);   // Required since the return type is different than standard ATLAS StatusCode.
    AppUtils::TOptions options(argc, argv);
    std::shared_ptr<TLapTimer> lapTimer;
    options.interprete();
    if (options.isHelpRequested()) return 0;
    if (options.isLogEnabled()) {
        lapTimer = std::make_shared<TLapTimer>();
        lapTimer->start("Initialisation");
    }

    ANA_CHECK(xAOD::Init(argv[0])); // Set up the job for xAOD access.
    if (options.enableMT()) { ROOT::EnableImplicitMT(options.NThreads()); }
    auto xdf = xAOD::DataSource::make_xAODdataFrame(options.fileNamePattern(),
                                                    xAOD::DataSource::EVENT_TREE_NAME,
                                                    lapTimer);

    // Extract vector variable from AuxContainer
    const auto readPt = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> ptVector{};
        for (const auto &p: *particles) {
            ptVector.push_back(p->pt());
        }
        return ptVector;
    };

    constexpr auto ptColumnName = "pt";
    auto ptData = xdf.Define(ptColumnName, readPt, {"Electrons"});    //Define a new column
    const TH1DModel &histModel = {"histElectronPt", "histElectronPt", 100, 0.0, 1500.0};
    auto histElectronPt = ptData.Histo1D(histModel, ptColumnName);

    // Access data
    const auto entries = histElectronPt->GetEntries();
    const auto mean = histElectronPt->GetMean();
    const auto sd = histElectronPt->GetStdDev();

    if (!options.isQuietModeEnabled()) {
        ANA_MSG_INFO("Histogram Electorn.pt: Total entries: " << std::setprecision(16) << entries);
        ANA_MSG_INFO("Mean: " << mean << " +/- " << sd);
    }

    if (options.isLogEnabled()) {
        lapTimer->stop();
        auto lapDurations = lapTimer->lapDurations();

        AppUtils::TLogFile logFile(options.logfileName());
        logFile.append(lapTimer->wallTime());
        logFile.appendLine(lapDurations);
    }

    return 0;
}
