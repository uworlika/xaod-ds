#include "xAODdataSource/TxAODdataSource.h"
#include "AppUtils/TOptions.h"

//AnalysisBase includes:
#include "xAODRootAccess/Init.h"
#include "xAODBase/IParticleContainer.h"
#include "AsgTools/MessageCheck.h"

int main(int argc, char *argv[]) {
    using namespace asg::msgUserCode;

    ANA_CHECK_SET_TYPE(int);   // Required since the return type is different than standard ATLAS StatusCode.
    AppUtils::TOptions options(argc, argv);
    options.interprete();
    if (options.isHelpRequested()) return 0;

    ANA_CHECK(xAOD::Init(argv[0])); // Set up the job for xAOD access.
    //if (options.enableMT()) { ROOT::EnableImplicitMT(options.NThreads()); }
    //auto xdf = xAOD::DataSource::make_xAODdataFrame(options.fileNamePattern(), xAOD::DataSource::EVENT_TREE_NAME);

    //constexpr auto fileNamePatternSmallDataSet = "${DATA}/TestCases/DAOD.HIGG2D2.EventInfo.Electrons.16Events.root";
    constexpr auto fileNamePattern = "${DATA}/data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213/*";
    auto xdf = xAOD::DataSource::make_xAODdataFrame(fileNamePattern, xAOD::DataSource::EVENT_TREE_NAME);

    const auto getSize = [](const xAOD::IParticleContainer *particles) { return static_cast<uint>(particles->size()); };
    const auto getEventIndex = [](const int slot, ULong64_t entry) { (void)slot; return entry; };

    auto particleDistributions = xdf.DefineSlotEntry("eventIndex", getEventIndex)
        .Define("electronsCount", getSize, {"Electrons"})
        .Define("muonsCount", getSize, {"Muons"})
        .Define("photonsCount", getSize, {"Photons"});

    const auto outFileName = "~/MT/xAODdataSource/run/Test.root";
    particleDistributions.Snapshot(xAOD::DataSource::EVENT_TREE_NAME, outFileName,
                                   {"eventIndex", "electronsCount", "muonsCount", "photonsCount"});

    return 0;
}
