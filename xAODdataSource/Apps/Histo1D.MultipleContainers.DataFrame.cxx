#include "xAODdataSource/TxAODdataSource.h"
#include "AppUtils/TOptions.h"

//AnalysisBase includes:
#include "xAODRootAccess/Init.h"
#include "xAODBase/IParticleContainer.h"
#include "AsgTools/MessageCheck.h"
#include "AppUtils/TLogFile.h"

int main(int argc, char *argv[]) {
    using namespace asg::msgUserCode;
    using TH1DModel = ROOT::RDF::TH1DModel;
    using TLapTimer = xAOD::DataSource::TLapTimer;

    ANA_CHECK_SET_TYPE(int);   // Required since the return type is different than standard ATLAS StatusCode.
    AppUtils::TOptions options(argc, argv);
    std::shared_ptr<TLapTimer> lapTimer;
    options.interprete();
    if (options.isHelpRequested()) return 0;
    if (options.isLogEnabled()) {
        lapTimer = std::make_shared<TLapTimer>();
        lapTimer->start("Initialisation");
    }

    ANA_CHECK(xAOD::Init(argv[0])); // Set up the job for xAOD access.
    if (options.enableMT()) { ROOT::EnableImplicitMT(options.NThreads()); }
    auto xdf = xAOD::DataSource::make_xAODdataFrame(options.fileNamePattern(),
                                                    xAOD::DataSource::EVENT_TREE_NAME,
                                                    lapTimer);

    // Extract vector variables from AuxContainer
    const auto pt = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->pt()); }
        return v;
    };

    const auto eta = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->eta()); }
        return v;
    };

    const auto phi = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->phi()); }
        return v;
    };

    const auto m = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->m()); }
        return v;
    };

    const auto e = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->e()); }
        return v;
    };

    const auto rapidity = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->rapidity()); }
        return v;
    };

    const auto beta = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Beta()); }
        return v;
    };

    const auto gamma = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Gamma()); }
        return v;
    };

    const auto mag = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Mag()); }
        return v;
    };

    const auto px = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Px()); }
        return v;
    };

    const auto py = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Py()); }
        return v;
    };

    const auto pz = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> v{};
        for (const auto &p: *particles) { v.push_back(p->p4().Pz()); }
        return v;
    };

    //Define columns and histograms
    //Todo: Investigate suitable pre-processing to eliminate hardcoded container list.
    std::vector<std::string> containerNames{"Muons", "Photons", "Electrons"};

    std::string columnName;
    TH1DModel histModel;
    constexpr unsigned int nBins = 128;
    constexpr double xLow = 0.0, xUp = 0.0;

    std::vector<ROOT::RDF::RResultPtr<::TH1D>> histograms{};
    for (auto &containerName : containerNames) {
        columnName = containerName + "_pt";
        auto ptData = xdf.Define(columnName, pt, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(ptData.Histo1D(histModel, columnName));

        columnName = containerName + "_eta";
        auto etaData = xdf.Define(columnName, eta, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(etaData.Histo1D(histModel, columnName));

        columnName = containerName + "_phi";
        auto phiData = xdf.Define(columnName, phi, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(phiData.Histo1D(histModel, columnName));

        columnName = containerName + "_m";
        auto mData = xdf.Define(columnName, m, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(mData.Histo1D(histModel, columnName));

        columnName = containerName + "_e";
        auto eData = xdf.Define(columnName, e, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(eData.Histo1D(histModel, columnName));

        columnName = containerName + "_rapidity";
        auto rapidityData = xdf.Define(columnName, rapidity, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(rapidityData.Histo1D(histModel, columnName));

        columnName = containerName + "_beta";
        auto betaData = xdf.Define(columnName, beta, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(betaData.Histo1D(histModel, columnName));

        columnName = containerName + "_gamma";
        auto gammaData = xdf.Define(columnName, gamma, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(gammaData.Histo1D(histModel, columnName));

        columnName = containerName + "_mag";
        auto magData = xdf.Define(columnName, mag, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(magData.Histo1D(histModel, columnName));

        columnName = containerName + "_px";
        auto pxData = xdf.Define(columnName, px, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(pxData.Histo1D(histModel, columnName));

        columnName = containerName + "_py";
        auto pyData = xdf.Define(columnName, py, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(pyData.Histo1D(histModel, columnName));

        columnName = containerName + "_pz";
        auto pzData = xdf.Define(columnName, pz, {containerName});
        histModel = TH1DModel(columnName.data(), columnName.data(), nBins, xLow, xUp);
        histograms.emplace_back(pzData.Histo1D(histModel, columnName));
    }

    if (!options.isQuietModeEnabled()) {
        std::cout << std::setw(20) << std::left << "Histogram" << std::setw(16) << "Entries" << "Mean\n";
    }

    for (auto &h: histograms) {     //Access data, Trigger event loop
        const auto histName = h->GetName();
        const auto entries = h->GetEntries();
        const auto mean = h->GetMean();

        if (options.isQuietModeEnabled()) continue;
        std::cout << std::setw(20) << std::left << histName
                  << std::setw(16) << entries << mean << "\n";
    }

    if (options.isLogEnabled()) {
        lapTimer->stop();
        auto lapDurations = lapTimer->lapDurations();

        AppUtils::TLogFile logFile(options.logfileName());
        logFile.append(lapTimer->wallTime());
        logFile.appendLine(lapDurations);
    }

    return 0;
}