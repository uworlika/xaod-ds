#ifndef xAODdataSource_TEventCursor_H
#define xAODdataSource_TEventCursor_H

// xAODdataSource includes:
#include "xAODdataSource/THandle.h"

// AnalysisBase includes:
#include "xAODRootAccess/TEvent.h"

// ROOT includes:
#include "TObject.h"
#include "RStringView.h"

// STL includes:
#include <set>

namespace xAOD {
namespace DataSource {

class TEventCursor {

public:
    ~TEventCursor();

    void initialise(std::unique_ptr<TEvent> event);

    void finalise();

    void *columnReader(std::string_view columnName, const std::type_info &typeInfo);

    int setEntry(const ULong64_t entry);

    inline bool isInitialized() { return m_event != nullptr; }

private:
    std::unique_ptr<TEvent> m_event = nullptr;
    std::set<std::unique_ptr<IHandle>> m_columnHandles;

};


} // name-space DataSource
} // name-space xAOD
#endif // xAODdataSource_TEventCursor_H