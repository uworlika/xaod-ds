#ifndef xAODdataSource_LapTimer_H
#define xAODdataSource_LapTimer_H

// ROOT includes:
#include <TStopwatch.h>

// STL includes:
#include <deque>
#include <chrono>
#include <string>

namespace xAOD {
namespace DataSource {

using seconds = Double_t; //Floating point representation of duration in seconds.
using TimeLap_t = std::pair<std::string, seconds>;

class TLapTimer {

public:
    explicit TLapTimer() = default;

    TLapTimer(const TLapTimer &rhs) = delete;

    TLapTimer(TLapTimer &&rhs) = delete;

    TLapTimer &operator=(const TLapTimer &rhs) = delete;

    TLapTimer &operator=(TLapTimer &&rhs) = delete;

    ~TLapTimer() = default;

    void start(std::string &&timeLapName);

    void split(std::string &&timeLapName);

    void stop();

    void reset();

    seconds wallTime();

    std::vector<std::string> lapNames() const;

    std::vector<seconds> lapDurations() const;

private:
    TStopwatch m_stopWatch{};
    TStopwatch m_wallClock{};
    std::deque<TimeLap_t> m_timeLaps{};
};

} // name-space DataSource
} // name-space xAOD
#endif // xAODdataSource_LapTimer_H