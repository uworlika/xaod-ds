#ifndef xAODdataSource_THandle_H
#define xAODdataSource_THandle_H

// AnalysisBase includes:
#include "AsgTools/StatusCode.h"
#include "xAODRootAccess/TEvent.h"

// ROOT includes:
#include "RStringView.h"

namespace xAOD {
namespace DataSource {

//Pure abstract interface for Handle objects
class IHandle {
public:
    virtual ~IHandle() = default;

    virtual std::string_view columnName() const = 0;

    virtual StatusCode update() = 0;

    virtual void *reader() const = 0;

    virtual bool operator==(const IHandle &rhs) const = 0;

    virtual bool operator!=(const IHandle &rhs) const = 0;

    virtual bool operator<(const IHandle &rhs) const = 0;

    virtual bool operator>(const IHandle &rhs) const = 0;

    virtual bool operator<=(const IHandle &rhs) const = 0;

    virtual bool operator>=(const IHandle &rhs) const = 0;
};

std::unique_ptr<IHandle> makeHandle(std::string_view columnName,
                                    const std::type_info &typeInfo,
                                    const std::unique_ptr<TEvent> &event);

std::string extractNormalisedTypeName(const std::type_info &typeInfo);


///\brief This class encapsulates the pointer-to-pointer-pointer relationship of memory addresses involved
/// in the RDataFrame's data access using it's ColumnReaders. Concretely, a handle object "links" a data
/// location in memory with reader. This allows data source to dynamically change the data locations during
/// event loop, while TDataFrame can safely use reader-end. The 'middle link' is secured to remain unchanged.
/// Encapsulating these three pointers together, the handle prevents any accidental side effects.
/// In addition, the handle also mimics the 'move-only' semantic of unique pointer for ownership transfer.
/// The current implementation reflects read-only handles, yet the name is left to be generic, since it is
/// unclear if data source may/should also support write-handles [and tool-handles?].
template<typename T>
class THandle : public IHandle {
public:
    explicit THandle(std::string_view columnName, const std::unique_ptr<TEvent> &event);

    THandle(const THandle &address) = delete;           //Disable copy
    THandle(THandle &&rhs) noexcept;

    THandle &operator=(THandle &&rhs) noexcept;

    ~THandle() override;

    std::string_view columnName() const override;

    StatusCode update() override;

    void *reader() const override;

    bool operator==(const IHandle &rhs) const override;

    bool operator!=(const IHandle &rhs) const override;

    bool operator<(const IHandle &rhs) const override;

    bool operator>(const IHandle &rhs) const override;

    bool operator<=(const IHandle &rhs) const override;

    bool operator>=(const IHandle &rhs) const override;

private:
    std::string m_columnName;
    const std::unique_ptr<TEvent> &m_event;
    const T *m_address{nullptr};
    const T **m_link{nullptr};
    const T ***m_reader{nullptr};
};

} // name-space DataSource
} // name-space xAOD
#endif // xAODdataSource_THandle_H