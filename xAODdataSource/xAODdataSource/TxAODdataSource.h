#ifndef xAODdataSource_xAODdataSource_H
#define xAODdataSource_xAODdataSource_H

#include "xAODdataSource/TEventCursor.h"
#include "xAODdataSource/TLapTimer.h"

// AnalysisBase includes:
#include "xAODRootAccess/TEvent.h"

// ROOT includes:
#include <TObject.h>
#include <ROOT/RDataFrame.hxx>
#include <ROOT/RDataSource.hxx>

// STL includes:
#include <map>
#include <chrono>

namespace xAOD {
namespace DataSource {

constexpr auto EVENT_TREE_NAME = "CollectionTree";
using RDataSource = ROOT::RDF::RDataSource;
using RDataFrame = ROOT::RDataFrame;

ROOT::RDataFrame make_xAODdataFrame(std::string_view fileNamePattern,
                                    std::string_view treeName = EVENT_TREE_NAME,
                                    std::shared_ptr<TLapTimer> lapTimer = nullptr);

std::unique_ptr<TChain> makeDataChain(std::string_view fileNamePattern, std::string_view treeName);

class TxAODdataSource final : public ROOT::RDF::RDataSource {

public:
    explicit TxAODdataSource(std::string_view fileNamePattern,
                             std::string_view eventTreeName = EVENT_TREE_NAME,
                             std::shared_ptr<TLapTimer> lapTimer = nullptr);

    ~TxAODdataSource() override;

    void SetNSlots(unsigned int nSlots) override;

    void Initialise() override;

    void Finalise() override;

    bool HasColumn(std::string_view columnName) const override;

    const std::vector<std::string> &GetColumnNames() const override;

    std::string GetTypeName(std::string_view) const override;

    std::vector<std::pair<ULong64_t, ULong64_t>> GetEntryRanges() override;

    bool SetEntry(unsigned int slot, ULong64_t entry) override;

    void InitSlot(unsigned int slot, ULong64_t firstEntry) override;

    void FinaliseSlot(unsigned int slot) override;

private:
    const std::string m_fileNamePattern;
    const std::string m_eventTreeName;
    std::shared_ptr<TLapTimer> m_lapTimer;
    std::atomic<unsigned int> m_NPendingTasks{0};

    ULong64_t m_size{};
    std::unordered_map<std::string, std::string> m_columnClassMap{};
    std::vector<std::string> m_columnNames{};
    unsigned int m_NSlots{};
    std::vector<std::unique_ptr<TEventCursor>> m_eventCursors{};
    std::vector<std::pair<ULong64_t, ULong64_t>> m_entryRanges{};

    std::vector<void *> GetColumnReadersImpl(std::string_view columnName, const std::type_info &typeInfo) override;

    void InitMetaData();
};

} // name-space DataSource
} // name-space xAOD
#endif // xAODdataSource_xAODdataSource_H