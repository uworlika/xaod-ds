#include "xAODdataSource/TxAODdataSource.h"
#include "AppUtils/TOptions.h"

// AnalysisBase includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODRootAccess/Init.h"

// ROOT includes:
#include <ROOT/RDFHistoModels.hxx>

// GoogleTest includes:
#include <gtest/gtest.h>
#include <gmock/gmock.h>

namespace xAOD {
namespace DataSource {
namespace Test {

using TH1DModel = ROOT::RDF::TH1DModel;
constexpr auto fileNamePatternSmallDataSet = "${DATA}/TestCases/DAOD.HIGG2D2.EventInfo.Electrons.16Events.root";
constexpr auto fileNamePattern = "${DATA}/data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213/*";
constexpr auto ptColumnName = "pt";

//Extract vector variable from AuxContainer
const auto readPt = [](const xAOD::IParticleContainer *particles) {
    std::vector<double> ptVector{};
    for (const auto &p: *particles) {
        ptVector.push_back(p->pt());
    }
    return ptVector;
};

TEST(EventInfoTest, ReadEventInfo) {
    ROOT::EnableImplicitMT();
    xAOD::Init().ignore();
    auto xdf = make_xAODdataFrame(fileNamePattern);

    //Define transient columns mapping to xAOD functions EventInfo::runNumber(), EventInfo::eventNumber()
    const auto branchName = "EventInfo";    //Todo: Provide constructor for default branch(es).
    auto runNumberData = xdf.Define("RunNumber", [](const EventInfo *ei) { return ei->runNumber(); }, {branchName});
    auto eventNumberData = xdf.Define("EventNumber",
                                      [](const EventInfo *ei) { return ei->eventNumber(); }, {branchName});

    //Apply lazy actions
    auto eventNumberMin = eventNumberData.Min({"EventNumber"});
    auto eventNumberMax = eventNumberData.Max({"EventNumber"});
    auto runNumberHist = runNumberData.Histo1D("RunNumber");

    //Access data
    EXPECT_EQ(346749618ULL, eventNumberMin.GetValue());
    EXPECT_EQ(2378205328ULL, eventNumberMax.GetValue());
    EXPECT_EQ(328263ULL, runNumberHist->GetMean());
}

TEST(VectorContainerTest, ReadSingleVectorContainer) {
    ROOT::EnableImplicitMT();
    xAOD::Init().ignore();
    auto xdf = make_xAODdataFrame(fileNamePattern);
    auto ptData = xdf.Define(ptColumnName, readPt, {"Electrons"});

    auto eventCount = ptData.Count();
    auto ptMin = ptData.Min(ptColumnName);
    auto ptMax = ptData.Max(ptColumnName);

    EXPECT_EQ(353793ULL, eventCount.GetValue());
    EXPECT_DOUBLE_EQ(0.0, ptMin.GetValue());
    EXPECT_DOUBLE_EQ(1199963.5, ptMax.GetValue());
}

TEST(AnalysisFeatureTest, Histo1D) {
    ROOT::EnableImplicitMT();
    xAOD::Init().ignore();
    auto xdf = make_xAODdataFrame(fileNamePattern);
    auto ptData = xdf.Define(ptColumnName, readPt, {"Electrons"});    //Define a new column

    const TH1DModel &histModel = {"histElectronPt", "histElectronPt", 100, 0.0, 1500.0};
    auto histElectronPt = ptData.Histo1D(histModel, ptColumnName);

    EXPECT_EQ(2967698, histElectronPt->GetEntries());
    EXPECT_DOUBLE_EQ(0.0, histElectronPt->GetMinimum());
    EXPECT_DOUBLE_EQ(1684.0, histElectronPt->GetMaximum());
    EXPECT_DOUBLE_EQ(1349.476856700301, histElectronPt->GetMean());
}

TEST(AnalysisFeatureTest, FilterMinMax) {
    ROOT::EnableImplicitMT();
    xAOD::Init().ignore();
    auto xdf = make_xAODdataFrame(fileNamePattern);
    auto totalEvents = xdf.Count();

    //Define a limiting function for pt variable.
    constexpr auto ptThreshold = 10000.0;
    const auto ptCut = [](const xAOD::IParticleContainer *particles) {
        std::vector<double> ptVector{};
        for (const auto &p: *particles) {
            if (p->pt() >= ptThreshold) { ptVector.push_back(p->pt()); };
        }
        return ptVector;
    };

    //Define a transient column to hold filtered data.
    constexpr auto ptCutColumn = "ptCutVector";
    auto ptCutData = xdf.Define(ptCutColumn, ptCut, {"Electrons"});

    //Define a lazy event filter and use it to derive more analysis variables.
    auto filteredEvents = ptCutData.Filter("ptCutVector.size() > 2");
    auto filteredEventCount = filteredEvents.Count();
    auto ptCutMin = filteredEvents.Min(ptCutColumn);
    auto ptCutMax = filteredEvents.Max(ptCutColumn);

    //Access data to evaluate all actions.
    EXPECT_EQ(353793ULL, totalEvents.GetValue());
    EXPECT_EQ(114146ULL, filteredEventCount.GetValue());
    EXPECT_DOUBLE_EQ(10000.017578125, ptCutMin.GetValue());
    EXPECT_DOUBLE_EQ(1199963.5, ptCutMax.GetValue());
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

} // namespace Test
} // namespace DataSource
} // namespace xAOD