#include "xAODdataSource/TxAODdataSource.h"

// AnalysisBase includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODdataSource/TxAODdataSource.h"

// GoogleTest includes:
#include <gtest/gtest.h>
#include <gmock/gmock.h>

//TODO: Register a common test executable script with atlas_add_test.

namespace xAOD {
namespace DataSource {
namespace Test {

using namespace ::testing;
using RDataFrame = ROOT::RDataFrame;
constexpr auto fileNamePattern = "${DATA}/TestCases/DAOD.HIGG2D2.EventInfo.Electrons.16Events.root";//Todo: Mock files

TEST(MakeDataFrameTest, ThrowOnEmptyFilePattern){
    ASSERT_THROW(make_xAODdataFrame(""), std::invalid_argument);
}

TEST(MakeDataFrameTest, DISABLED_ThrowOnInvalidFileName) {
    ASSERT_THROW(make_xAODdataFrame("NoSuchFile"), std::runtime_error);
}

TEST(MakeDataFrameTest, ThrowOnEmptyTreeName){
    ASSERT_THROW(make_xAODdataFrame(fileNamePattern, ""), std::invalid_argument);
}

TEST(MakeDataFrameTest, MakeDataFrameWithValidFileNamePattern) {
    ASSERT_NO_THROW(make_xAODdataFrame(fileNamePattern));
}

TEST(MakeDataFrameTest, ValidateSizePerInputFile) {
    RDataFrame xdf = make_xAODdataFrame(fileNamePattern);
    EXPECT_EQ(16ULL /*expectedEventCount*/, xdf.Count().GetValue());
}

TEST(SetNSlotsTest, ThrowOnZeroSlots) {
    TxAODdataSource xds(fileNamePattern);
    ASSERT_THROW(xds.SetNSlots(0), std::invalid_argument);
}

TEST(SetNSlotsTest, SetOneSlots) {
    TxAODdataSource xds(fileNamePattern);
    ASSERT_NO_THROW(xds.SetNSlots(1));
}

TEST(SetNSlotsTest, SetMultipleSlots) {
    TxAODdataSource xds(fileNamePattern);
    ASSERT_NO_THROW(xds.SetNSlots(4));
}

TEST(HasColumnTest, ReturnFalseOnMissingColumn) {
    TxAODdataSource xds(fileNamePattern);
    ASSERT_FALSE(xds.HasColumn("NoSuchColumn"));
}

TEST(HasColumnTest, ReturnTrueOnExistingColumn) {
    TxAODdataSource xds(fileNamePattern);
    ASSERT_TRUE(xds.HasColumn("EventInfo"));
}

TEST(GetColumnNamesTest, MultipleColumnNames) {
    constexpr auto inputFiles = "${DATA}/TestCases/DAOD.HIGG2D2.EventInfo.16Events.root";
    TxAODdataSource xds(inputFiles);
    const auto &columnNames = xds.GetColumnNames();
    ASSERT_THAT(columnNames, SizeIs(4));
    EXPECT_THAT(columnNames, UnorderedElementsAre("EventInfo",
                                                  "EventInfoAux.",
                                                  "EventInfoAuxDyn.streamTagDets",
                                                  "EventInfoAuxDyn.streamTagRobs"));
}

TEST(GetTypeNameTest, ThrowOnMissingColumn) {
    TxAODdataSource xds(fileNamePattern);
    ASSERT_THROW(xds.GetTypeName("NoSuchColumn"), std::runtime_error);
}

TEST(GetTypeNameTest, ReturnTypeNameOnValidColumnName) {
    TxAODdataSource xds(fileNamePattern);
    ASSERT_THAT(xds.GetTypeName("EventInfo"), StrEq("xAOD::EventInfo_v1"));
}

TEST(GetColumnReadersTest, ThrowOnMissingColumn) {
    TxAODdataSource xds(fileNamePattern);
    ASSERT_THROW(xds.GetColumnReaders<int>("NoSuchColumn"), std::runtime_error);
}

// Validates that columnName and type are consistent.
TEST(GetColumnReadersTest, DISABLED_ThrowOnMissMatchedType) {
    constexpr auto inputFiles = "${DATA}/TestCases/DAOD.HIGG2D2.EventInfo.16Events.root";
    TxAODdataSource xds(inputFiles);
    constexpr auto columnName = "EventInfo";
    ASSERT_TRUE(xds.HasColumn(columnName));     // Simulate valid columnName but invalid type.
    ASSERT_THROW(xds.GetColumnReaders<int>(columnName), std::runtime_error);
}

TEST(GetEntryRangesTest, ReturnMultipleRangesForMultipleClusters) {
    constexpr auto inputFiles = "${DATA}/data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213/*";
    TxAODdataSource xds(inputFiles);
    xds.SetNSlots(1);
    xds.Initialise();
    const auto ranges = xds.GetEntryRanges();
    ASSERT_EQ(3563, ranges.size());
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

} // namespace Test
} // namespace DataSource
} // namespace xAOD