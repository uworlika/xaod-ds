# **An implementation of ROOT's RDataSource interface for reading the ATLAS xAOD files within the ROOT framework.**

This repository currently contains three packages.  
**1. xAODdataSource:** A collection of interfaces and classes which includes
implementation of ROOT's TDataSource interface and supporting classes which
interact with ATLAS event store.  
**2. EventLoopTests:** A set of test applications, to demonstrate use cases of
ATLAS EventLoop tool, as used for accessing ATLAS EDM objects using AnaAlgorithm
framework. [https://atlassoftwaredocs.web.cern.ch/ABtutorial/]  
**3. AppUtils**: A set of interfaces and utility classes to support test
applications used by above two packages.

Each package has directory structure as below:  
**PackageName:** Interfaces, public headers of package.  
**Root:** Implementation, source files.  
**Test:** Unit tests and Integration tests.  
**Apps:** Standalone test applications.  

## **Setup instructions**  
**Build Prerequisites**  
ROOT release >= 6.14/00, see https://root.cern.ch/build-prerequisites for ROOT dependencies.  
AnalysisBase 21.2.xx  
**Note:** For linking current AnalysisBase versions (as of 21.2.39) a local update is required in PerfStats interface.  
This is due to some newer virtual functions declared in ROOT 6.14 interfaces, those are yet to be implemented in AnalysisBase code.  
A temporory code patch currently needs to be locally applied in /athena/Event/xAOD/xAODCore/xAODCore/tools/PerfStats.h before compiling AnalysisBase 

```c++
// Functions from newer interfaces of ROOT 6.14, A workaround to compile AnalysisBase. Not implemented here.
 #if ROOT_VERSION_CODE >= ROOT_VERSION( 6, 14, 00 )
      inline void PrintBasketInfo(Option_t *option = "") const {};
      inline void SetLoaded(TBranch *b, size_t basketNumber) {};
      inline void SetLoaded(size_t bi, size_t basketNumber) {};
      inline void SetLoadedMiss(TBranch *b, size_t basketNumber) {};
      inline void SetLoadedMiss(size_t bi, size_t basketNumber) {};
      inline void SetMissed(TBranch *b, size_t basketNumber) {};
      inline void SetMissed(size_t bi, size_t basketNumber) {};
      inline void SetUsed(TBranch *b, size_t basketNumber) {};
      inline void SetUsed(size_t bi, size_t basketNumber) {};
      inline void UpdateBranchIndices(TObjArray *branches) {};
 #endif  
```
        
  
## **Setup on CentOS7**  
**Note:** Setup on CentOS/slc6 needs cvmfs access and basic ATLAS environment ${ATLAS_LOCAL_ROOT_BASE}  
See : [https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/] for more info.  

**Build/Install ROOT and prepare environment**  
mkdir ROOTBuild cd ROOTBuild  
setupATLAS && lsetup git  
git clone http://github.com/root-project/root.git  
git checkout -b [vx-xx-xx] [vx-xx-xx]  where [vx-xx-xx] refers to ROOT version tag [e.g v6-14-02]  
  
asetup none,gcc62 --cmakesetup  
cmake -Dcxx14=ON -Dall=ON -Dbuiltin_xrootd=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo  \  
-DCMAKE_INSTALL_PREFIX=/InstallPath/ROOT/vx-xx-xx ../root  
cmake --build . -- install -jx  
source /InstallPath/ROOT/vx-xx-xx/bin/thisroot.sh

**Setup athena repository**  
mkdir AnalysisBase, cd AnalysisBase  
git clone https://:@gitlab.cern.ch:8443/[user_name]/athena.git  
cd athena  
git remote add upstream https://:@gitlab.cern.ch:8443/atlas/athena.git  
git fetch upstream  
git checkout release/21.2.xx  
  
**Note:** Use 21.2 for latest version.  
See : [https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/#building-your-own-release-optional--advanced]  

**Note:** Current AnalysisBase build scripts force install ROOT from AnalysisBase  
release if any of these external packages are built [Python, XRootD, DCAP, LibXML2, Davix]  
To prevent force install built-in ROOT, currently a workaround is used by  
modifying /athena/Build/AtlasBuildScripts/build_atlasexternals.sh with  
following cmake parameters.  

```bash  
-DATLAS_BUILD_PYTHON:BOOL=FALSE -DATLAS_BUILD_XROOTD:BOOL=FALSE \  
-DATLAS_BUILD_DCAP:BOOL=FALSE -DATLAS_BUILD_LIBXML2:BOOL=FALSE  \  
-DATLAS_BUILD_DAVIX:BOOL=FALSE    
```
        
**Build AnalysisBase**  
export MAKEFLAGS=-jx  
asetup none, gcc62 --cmakesetup  
./athena/Projects/AnalysisBase/build_externals.sh -c  
./athena/Projects/AnalysisBase/build.sh -acmi  
asetup AnalysisBase,21.2.xx --releasesarea=/home/.../AnalysisBase/build/install  
  
**Setup xAODdataSource**  
mkdir xAODdataSource, cd xAODdataSource  
Clone the repository and checkout suitable tag  
git checkout -b [x.x.xx] [x.x.xx]  where [x.x.xx] refers to a git tag  
mkdir build, cd build  
cmake ../xaod-ds  
cmake --build . -- -jx  
source ./x86_64-centos7-gcc62-opt/setup.sh  
  
  
## **Setup on Ubuntu 18.04.LTS**  
  
**Build/Install ROOT and prepare environment**  
mkdir ROOTBuild cd ROOTBuild  
git clone http://github.com/root-project/root.git  
git checkout -b [vx-xx-xx] [vx-xx-xx]  where [vx-xx-xx] refers to ROOT version tag [e.g v6-14-02]  

cmake -Dcxx14=ON -Dall=ON -Dbuiltin_xrootd=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo  \  
-DCMAKE_INSTALL_PREFIX=/InstallPath/ROOT/vx-xx-xx ../root  
cmake --build . -- install -jx  
source /InstallPath/ROOT/vx-xx-xx/bin/thisroot.sh

**Setup athena repository**  
mkdir AnalysisBase, cd AnalysisBase  
git clone https://:@gitlab.cern.ch:8443/[user_name]/athena.git  
cd athena  
git remote add upstream https://:@gitlab.cern.ch:8443/atlas/athena.git  
git fetch upstream  
git checkout release/21.2.xx  
  
**Note:** Use 21.2 for latest version.  
See : [https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/#building-your-own-release-optional--advanced]  

**Note:** Current AnalysisBase build scripts force install ROOT from AnalysisBase  
release if any of these external packages are built [Python, XRootD, DCAP, LibXML2, Davix]  
To prevent force install built-in ROOT, currently a workaround is used by  
modifying /athena/Build/AtlasBuildScripts/build_atlasexternals.sh with  
following cmake parameters.  
```bash  
-DATLAS_BUILD_PYTHON:BOOL=FALSE -DATLAS_BUILD_XROOTD:BOOL=FALSE \  
-DATLAS_BUILD_DCAP:BOOL=FALSE -DATLAS_BUILD_LIBXML2:BOOL=FALSE  \  
-DATLAS_BUILD_DAVIX:BOOL=FALSE  
```
        

**Build AnalysisBase**  
export MAKEFLAGS=-jx  
./athena/Projects/AnalysisBase/build_externals.sh -c  
./athena/Projects/AnalysisBase/build.sh -acmi  
source /home/.../AnalysisBase/build/install/AnalysisBase/21.2.39/InstallArea/x86_64-ubuntu1804-gcc7-opt/setup.sh    
  
**Setup xAODdataSource**  
mkdir xAODdataSource, cd xAODdataSource  
Clone the repository and checkout suitable tag  
git checkout -b [x.x.xx] [x.x.xx]               where [x.x.xx] refers to a git tag  
mkdir build, cd build  
cmake ../xaod-ds  
cmake --build . -- -jx  
source ./x86_64-centos7-gcc62-opt/setup.sh  

**Test if everything went well**  
mkdir ../run, cd ../run
export DATA=[base_directory_of_xaod_data_files]  
UnitTests.xAODdataSource  
IntTests.xAODdataSource  

**A sample test run of xDataFrame test appication**  
ReadAuxElement.DataFrame --input-files "$DATA/data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213/*"  
xAOD::Init                INFO    Environment initialised for data access  
xAODdataSource::Initialize Distributed [353793] events to [4] slots  
xAODdataSource::Initialize Appended remainder to last Range  
AsgTools_UserCode        INFO    EventInfo.eventNumber Range: [346749618, 2378205328]  
AsgTools_UserCode        INFO    Histogram EventInfo.runNumber:  
AsgTools_UserCode        INFO    Total entries: 353793  
AsgTools_UserCode        INFO    Range: [0, 353793]  
AsgTools_UserCode        INFO    Mean: 328263 StdDev: 0.8739181369878131

**And corresponding test run of EventLoop test appication**  
ReadAuxElement.EventLoop --input-files "$DATA/data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213/*" --out-dir out  
xAOD::Init                INFO    Environment initialised for data access  
Running sample: data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213  
Processing File: file:///.../Data/data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213/DAOD_HIGG2D2.11597434._000104.pool.root.1  
Processing File: file:///.../Data/data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213/DAOD_HIGG2D2.11597434._000468.pool.root.1  
Processed 10000 events  
...  
Processed 340000 events  
Processing File: file:///.../Data/data17_13TeV.00328263.physics_Main.deriv.DAOD_HIGG2D2.f836_m1824_p3213/DAOD_HIGG2D2.11597434._000520.pool.root.1  
Processed 350000 events  
ReadAuxElementAnaAlgor...INFO    EventNumber Range: [346749618, 2378205328]  

worker finished:  
Real time 0:00:05, CP time 5.510  
AsgTools_UserCode        INFO    Histogram EventInfo.runNumber:  
AsgTools_UserCode        INFO    Total entries: 353793  
AsgTools_UserCode        INFO    Range: [0, 353793]  
AsgTools_UserCode        INFO    Mean: 328263 StdDev: 0.8739181369878131  
xAOD::TFileAccessTracer   INFO    Sending file access statistics to http://rucio-lb-prod.cern.ch:18762/traces/  

**There is more...**  
<pre><code>  ReadAuxElement.EventLoop --help  
Command line options:  
  -h [ --help ]                         Provide usage help  
  -i [ --input-files ] arg (=\*.root\*) Input filename pattern  
  -o [ --out-dir ] arg (=outDir)        Output directory  
  -m [ --enable-MT ] arg (=1)           [true/false, yes/no] Allows to disable ImpliciteMT feature  
                                        Default: true  
                                        When disabled, --threads option is ignored  
  -q [ --quiet ] [=arg(=1)] (=0)        [true/false, yes/no] Disables printing results to stdout  
                                        Default: false  
                                        Reduces verbosity in batch mode/performance tests  
  -t [ --threads ] arg (=0)             Limit number of threads used for ImpliciteMT feature  
                                        Default: Use all available threads  
  -e [ --max-events ] arg (=18446744073709551615)  
                                        Limit the number of events to process  
                                        Default: Process all events  
  -l [ --logfile ] arg                  Logfile name: Currently used for instrumentation of performance time steps.  
<code><pre>  
  
**Note:** Most of these options also work with xxx.DataFrame versions except for --max-events.  
Options those are not applicable [e.g. --enable-MT in serial EventLoop], are currently ignored silently. 