#include "AppUtils/TOptions.h"
#include "AnaAlgorithm/AnaAlgorithmConfig.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/Sample.h"
#include "xAODRootAccess/Init.h"
#include "AsgTools/MessageCheck.h"
#include <TH1.h>
#include <TFile.h>
#include <iomanip>

int main(int argc, char *argv[]) {
    using namespace asg::msgUserCode;
    ANA_CHECK_SET_TYPE(int);   // Set the return type and reporting category for ANA_CHECK

    AppUtils::TOptions options(argc, argv);
    options.interprete();
    if (options.isHelpRequested()) { return 0; }

    ANA_CHECK(xAOD::Init()); // Set up the job for xAOD access.

    // Scan subdirectories for data files using SampleHandler.
    const auto inFilesFullPath = options.fileNamePattern().data();
    const auto filePattern = AppUtils::trimParentPath(inFilesFullPath);
    const auto inputDirectory = AppUtils::extractParentPath(inFilesFullPath);

    SH::SampleHandler sh;
    SH::ScanDir().filePattern(filePattern).scan(sh, inputDirectory);
    sh.setMetaString("nc_tree", AppUtils::DEFAULT_TREE_NAME);

    // Configure job options.
    EL::Job job;
    job.sampleHandler(sh);
    if (options.maxEvents() < ULONG_LONG_MAX) {
        job.options()->setInteger(EL::Job::optMaxEvents, options.maxEvents()); //EL::Job supports only signed Int.
    }

    // Add algorithm to the job.
    EL::AnaAlgorithmConfig config;
    constexpr auto ALGO_TYPE_NAME = "ReadAuxElementAnaAlgorithm";
    config.setType(ALGO_TYPE_NAME); //AlgorithmFactory needs this for instantiation on each worker.
    config.setName(ALGO_TYPE_NAME); //AsgTools uses this for log messages.
    job.algsAdd(config);

    // Submit job using direct driver.
    EL::DirectDriver driver;
    driver.submit(job, options.outDirectory().data());

    // Fetch histogram data.
    const std::string sampleName = AppUtils::trimParentPath(inputDirectory);
    const auto histFileName = options.outDirectory().data() + std::string{AppUtils::HIST_FILE_PREFIX} +
        sampleName + std::string{AppUtils::ROOT_FILE_EXT};

    const auto histFile = std::make_unique<TFile>(histFileName.data());
    const auto histRunNumber = (TH1*)histFile->Get("histRunNumber");
    if(histRunNumber == nullptr){ ANA_MSG_ERROR("Histogram creation failed"); return -1; }

    auto defaultSettings = std::cout.flags();
    ANA_MSG_INFO("Histogram EventInfo.runNumber:");
    ANA_MSG_INFO("Total entries: " << std::setprecision(16) << histRunNumber->GetEntries());
    ANA_MSG_INFO("Range: [" << histRunNumber->GetMinimum() << ", " << histRunNumber->GetMaximum() << "]");
    ANA_MSG_INFO("Mean: " << histRunNumber->GetMean() << " StdDev: " << histRunNumber->GetStdDev());
    std::cout.flags(defaultSettings);

    return 0;
}