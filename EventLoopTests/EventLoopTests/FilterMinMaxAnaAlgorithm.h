#ifndef EventLoopTests_FilterAnaAlgorithm_H
#define EventLoopTests_FilterAnaAlgorithm_H

#include "AnaAlgorithm/AnaAlgorithm.h"
#include "TH1.h"

class FilterMinMaxAnaAlgorithm : public EL::AnaAlgorithm {

public:
    // Standard AnaAlgorithm constructor.
    FilterMinMaxAnaAlgorithm(const std::string &name, ISvcLocator *pSvcLocator);

    // This method is invoked before processing first event on each worker node.
    // Note: The method executes before any input files are connected.
    StatusCode initialize() override;

    // This method is invoked per event.
    StatusCode execute() override;

    // This method is invoked after the last event has been processed on each worker node.
    StatusCode finalize() override;

private:
    double m_ptThreshold;
    double m_ptCutMin;
    double m_ptCutMax;

    unsigned int m_totalEvents;
    unsigned int m_filteredEventsCount;
};

#endif