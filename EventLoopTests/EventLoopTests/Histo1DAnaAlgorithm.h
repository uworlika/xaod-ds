#ifndef EventLoopTests_Histo1DAnaAlgorithm_H
#define EventLoopTests_Histo1DAnaAlgorithm_H

#include "AnaAlgorithm/AnaAlgorithm.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/ElectronContainer.h"
#include "TH1.h"

class Histo1DAnaAlgorithm : public EL::AnaAlgorithm {

public:
    // Standard AnaAlgorithm constructor.
    Histo1DAnaAlgorithm(const std::string &name, ISvcLocator *pSvcLocator);

    // This method is invoked before processing first event on each worker node.
    // Note: The method executes before any input files are connected.
    StatusCode initialize() override;

    // This method is invoked per event.
    StatusCode execute() override;

    // This method is invoked after the last event has been processed on each worker node.
    StatusCode finalize() override;

private:

};

#endif