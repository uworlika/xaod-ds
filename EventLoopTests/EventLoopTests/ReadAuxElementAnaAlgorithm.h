#ifndef EventLoopTests_ReadAuxElementAnaAlgorithm_H
#define EventLoopTests_ReadAuxElementAnaAlgorithm_H

#include <TH1D.h>
#include "AnaAlgorithm/AnaAlgorithm.h"

class ReadAuxElementAnaAlgorithm : public EL::AnaAlgorithm {

public:
    // Standard AnaAlgorithm constructor.
    ReadAuxElementAnaAlgorithm(const std::string &name, ISvcLocator *pSvcLocator);

    // This method is invoked before processing first event on each worker node.
    // Note: The method executes before any input files are connected.
    StatusCode initialize() override;

    // This method is invoked per event.
    StatusCode execute() override;

    // This method is invoked after the last event has been processed on each worker node.
    StatusCode finalize() override;

private:
    unsigned long long m_eventNumberMin;
    unsigned long long m_eventNumberMax;
};

#endif