#ifndef EventLoopTests_EventLoopTestsDict_H
#define EventLoopTests_EventLoopTestsDict_H

// This file includes all the header files that you need to create
// dictionaries for.

#include "EventLoopTests/ReadAuxElementAnaAlgorithm.h"
#include "EventLoopTests/Histo1DAnaAlgorithm.h"
#include "EventLoopTests/FilterMinMaxAnaAlgorithm.h"

#endif
