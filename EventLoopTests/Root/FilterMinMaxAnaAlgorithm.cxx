#include "EventLoopTests/FilterMinMaxAnaAlgorithm.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODCore/AuxContainerBase.h"
#include "AsgTools/MessageCheck.h"
#include <iomanip>

// Standard AnaAlgorithm constructor.
FilterMinMaxAnaAlgorithm::FilterMinMaxAnaAlgorithm(const std::string& name, ISvcLocator* serviceLocator)
    : EL::AnaAlgorithm(name, serviceLocator) {

    declareProperty("PtThreshold", m_ptThreshold = 0.0, "Minimum electron pT [MeV]");
    declareProperty("PtCutMin", m_ptCutMin = std::numeric_limits<double>::max(), "Min observed ptCut [MeV]");
    declareProperty("PtCutMax", m_ptCutMax = std::numeric_limits<double>::min(), "Max observed ptCut [MeV]");
    declareProperty("TotalEvents", m_totalEvents = 0, "Number of total events in dataset");
    declareProperty("FilteredEventCount", m_filteredEventsCount = 0, "Number of filtered events");
}

// This method is invoked before processing first event on each worker node.
// Note: The method executes before any input files are connected.
StatusCode FilterMinMaxAnaAlgorithm::initialize() {
    ANA_MSG_DEBUG ("Initialize");
    return StatusCode::SUCCESS;
}

// This method is invoked per event.
StatusCode FilterMinMaxAnaAlgorithm::execute() {
    ANA_MSG_DEBUG ("Execute");
    ++m_totalEvents;

    // Create a transient vector for ptCut data.
    auto ptCutVector = std::vector<double>{};
    const xAOD::ElectronContainer* electrons = nullptr;
    ANA_CHECK(evtStore()->retrieve(electrons, "Electrons"));
    for (const auto e : *electrons) {
        if (e->pt() >= m_ptThreshold) { ptCutVector.push_back(e->pt()); } // Apply cut.
    }

    // Mimic some filter criterion.
    // In this case, skip all events, with less than 3 electrons with pt above the threshold value.
    if (ptCutVector.size() <= 2) return StatusCode::SUCCESS;

    // Count this event and update min/max.
    ++m_filteredEventsCount;
    for (const auto pt: ptCutVector) {
        m_ptCutMin = std::min(m_ptCutMin, pt);
        m_ptCutMax = std::max(m_ptCutMax, pt);
    }

    return StatusCode::SUCCESS;
}

// This method is invoked after the last event has been processed on each worker node.
StatusCode FilterMinMaxAnaAlgorithm::finalize() {
    ANA_MSG_DEBUG ("Finalize");

    // TODO: Currently assuming serial implementation. This really should be a binary reduce operation
    // after completion of event loop.
    auto defaultSettings = std::cout.flags();
    ANA_MSG_INFO("Total events: " << std::setprecision(16) << m_totalEvents);
    ANA_MSG_INFO("Filtered events: " << m_filteredEventsCount);
    ANA_MSG_INFO("Range|{Electron.pt >= minPt}: " << "[" << m_ptCutMin << ", " << m_ptCutMax << "]");
    std::cout.flags(defaultSettings);

    return StatusCode::SUCCESS;
}