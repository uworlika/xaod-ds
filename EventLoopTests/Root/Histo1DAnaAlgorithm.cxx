#include <iomanip>
#include "EventLoopTests/Histo1DAnaAlgorithm.h"
#include "AsgTools/MessageCheck.h"

// Standard AnaAlgorithm constructor.
Histo1DAnaAlgorithm::Histo1DAnaAlgorithm(const std::string& name, ISvcLocator* serviceLocator)
    : EL::AnaAlgorithm(name, serviceLocator) {

}

// This method is invoked before processing first event on each worker node.
// Note: The method executes before any input files are connected.
StatusCode Histo1DAnaAlgorithm::initialize() {
    ANA_MSG_DEBUG ("Initialize");
    ANA_CHECK(book(TH1D("histElectronPt", "histElectronPt", 100, 0.0, 1500.0)));
    return StatusCode::SUCCESS;
}

// This method is invoked per event.
StatusCode Histo1DAnaAlgorithm::execute() {
    ANA_MSG_DEBUG ("Execute");
    const xAOD::IParticleContainer* electrons = nullptr;
    ANA_CHECK (evtStore()->retrieve(electrons, "Electrons"));
    for(const auto& e : *electrons) {
        hist("histElectronPt")->Fill(e->pt());
    }
    return StatusCode::SUCCESS;
}

// This method is invoked after the last event has been processed on each worker node.
StatusCode Histo1DAnaAlgorithm::finalize() {
    ANA_MSG_DEBUG ("Finalize");
    return StatusCode::SUCCESS;
}