#include "EventLoopTests/ReadAuxElementAnaAlgorithm.h"
#include "xAODEventInfo/EventInfo.h"
#include <TH1.h>

// Standard AnaAlgorithm constructor.
ReadAuxElementAnaAlgorithm::ReadAuxElementAnaAlgorithm(const std::string& name, ISvcLocator* serviceLocator)
    : EL::AnaAlgorithm(name, serviceLocator) {

    declareProperty("EventNumberMin",
                    m_eventNumberMin = std::numeric_limits<unsigned long long>::max(), "Min{EventNumber}");

    declareProperty("EventNumberMax",
                    m_eventNumberMax = std::numeric_limits<unsigned long long>::min(), "Max{EventNumber}");

}

// This method is invoked before processing first event on each worker node.
// Note: The method executes before any input files are connected.
StatusCode ReadAuxElementAnaAlgorithm::initialize() {
    ANA_MSG_DEBUG ("Initialize");
    ANA_CHECK(book(TH1I("histRunNumber", "histRunNumber", 10, 328260, 328270)));
    return StatusCode::SUCCESS;
}

// This method is invoked per event.
StatusCode ReadAuxElementAnaAlgorithm::execute() {
    ANA_MSG_DEBUG ("Execute");

    // Access EventInfo.
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));

    // Update min/max.
    m_eventNumberMin = std::min(m_eventNumberMin, eventInfo->eventNumber());
    m_eventNumberMax = std::max(m_eventNumberMax, eventInfo->eventNumber());

    // Fill histogram.
    hist("histRunNumber")->Fill(eventInfo->runNumber());

    return StatusCode::SUCCESS;
}

// This method is invoked after the last event has been processed on each worker node.
StatusCode ReadAuxElementAnaAlgorithm::finalize() {
    ANA_MSG_DEBUG ("Finalize");

    // Display results. This really should be a custom merge operation on all workers.
    ANA_MSG_INFO("EventNumber Range: [" << m_eventNumberMin << ", " << m_eventNumberMax << "]\n");
    return StatusCode::SUCCESS;
}